<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->group(function() {
    Route::get('/u2f', 'U2FController@view')->name('fido');
});

Route::middleware(['auth', 'u2f'])->get('/', function () {
    return view('app');
});

Route::prefix('/api')->group(function() {
    Route::get('/u2f', 'U2FController@u2fInit');
    Route::post('/u2f/enroll', 'U2FController@u2fEnroll');
    Route::post('/u2f/login', 'U2FController@completeLogin');

    Route::middleware(['auth', 'u2f'])->group(function() {
        Route::get('/nodes', function() {
            $cl = new \GuzzleHttp\Client(['base_uri' => 'http://localhost:22646']);
            $ni = $cl->get('/');
            $cn = json_decode($ni->getBody());
            $cn->serial = "local";
            $cn->master = true;
            $an = $cl->get('/nodes');
            $all = json_decode($an->getBody());
            array_unshift($all, $cn);
            return $all;
        });

        Route::get('/apps/{serial}', function($serial) {
            $cl = new \GuzzleHttp\Client(['base_uri' => 'http://localhost:22646']);
            $an = $cl->get('/n/' . urlencode($serial) . '/apps');
            $re = json_decode($an->getBody()->getContents(), true);
            return $re;
        });

        Route::get('/apps/{serial}/{app}', function($serial, $app) {
            $cl = new \GuzzleHttp\Client(['base_uri' => 'http://localhost:22646']);
            $an = $cl->get('/n/' . urlencode($serial) . '/apps/' . urlencode($app) . '/dash');
            $re = json_decode($an->getBody()->getContents(), true);
            return $re;
        });

        Route::post('/apps/{serial}/{app}/{webhook}', function(\Illuminate\Http\Request $request, $serial, $app, $webhook) {
            $cl = new \GuzzleHttp\Client(['base_uri' => 'http://localhost:22646']);
            $an = $cl->post('/n/' . urlencode($serial) . '/apps/' . urlencode($app) . '/hook/' . urlencode($webhook), [
                'json' => $request->all()
            ]);
            $re = json_decode($an->getBody()->getContents(), true);
            return $re;
        });
    });


});

import Vue from 'vue';
import $ from 'jquery';
import App from "./U2FApp";
import axios from 'axios';
window.$ = $;
window.jQuery = $;
window.axios = axios;
require('bootstrap');

Vue.component('app', App);

var app = new Vue({
    el: "#app"
});

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>vCAN</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" />
    <style>
        html, body, #app {
            height: 100%;
        }
    </style>
</head>
<body>
    <div id="app"><app /></div>
    <script src="{{ asset('/js/app.js') }}"></script>
</body>
</html>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = ['id', 'email', 'handle', 'cert', 'key'];
}

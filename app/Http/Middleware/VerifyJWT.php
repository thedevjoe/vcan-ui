<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Storage;

class VerifyJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!env('JWT_TEST', false)) return $next($request);
        $key = env('JWT_KEY');
        $jwt = null;
        try {
            $jwt = JWT::decode(env('JWT_TEST'), $key, ['HS256']);
        } catch(\Exception $e) {
            return abort(401);
        }
        $request->request->add(['jwt' => $jwt]);
        return $next($request);
    }
}

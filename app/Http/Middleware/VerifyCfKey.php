<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Storage;

class VerifyCfKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!env('CF_VERIFY', false)) return $next($request);
        $keys = json_decode(Storage::disk('local')->get('cf_certs.json'), true);
        $jwt = null;
        try {
            $jwt = JWT::decode($request->header('Cf-Access-Jwt-Assertion'), $keys, ['RS256']);
        } catch(\Exception $e) {
            return abort(401);
        }
        $audInc = false;
        foreach($jwt->aud as $a) {
            if($a == env('CF_JWT_AUD')) $audInc = true;
        }
        if(!$audInc) return abort(401);
        $request->request->add(['jwt' => $jwt]);
        return $next($request);
    }
}

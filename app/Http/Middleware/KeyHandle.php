<?php

namespace App\Http\Middleware;

use App\Key;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class KeyHandle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $k = Auth::user();
        if($k == null || $request->jwt->email != $k->email) {
            //$request->session()->remove('key_handle');
            Auth::logout();
            return response([$request->jwt->email, Session::all()], 401);
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use App\Challenge;
use App\Key;
use App\User;
use App\Utils;
use Carbon\Carbon;
use Firehed\U2F\RegisterRequest;
use Firehed\U2F\RegisterResponse;
use Firehed\U2F\Server;
use Firehed\U2F\SignRequest;
use Firehed\U2F\SignResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class U2FController extends Controller
{
    public function view() {
        return view('u2f');
    }

    private $_server;
    public function server() {
        if($this->_server == null) return $this->_server = new Server();
        return $this->_server;
    }

    public function aid($request) {
        return 'https://' . urlencode($request->header('host'));
    }

    public function u2fInit(Request $request) {
        $s = $this->server();
        $aid = $this->aid($request);
        $s->setAppId($aid);
        $keys = Utils::getKeysForUser($request->jwt->email);
        $sign = [];
        $signIds = [];
        foreach($keys as $key) {
            $reqq = $s->generateSignRequest($key);
            $c = Challenge::getForEmail($request->jwt->email);
            $c->handle = base64_encode($reqq->getKeyHandleBinary());
            $c->expires_at = Carbon::now()->addMinutes(10);
            $c->save();
            $reqq->setChallenge($this->base64url_encode($c->challenge));
            array_push($signIds, $c->id);
            array_push($sign, $reqq);
        }
        if(count($keys) == 0) {
            $ch = Challenge::getForEmail($request->jwt->email);
            $ch->expires_at = Carbon::now()->addMinutes(10);
            $ch->save();
            $req = $s->generateRegisterRequest();
            $req->setChallenge($this->base64url_encode($ch->challenge));
            return ['action' => 'enroll', 'request' => $req, 'origin' => $aid, 'challenge_id' => $ch->id, 'sign' => $sign, 'signIds' => $signIds];
        } else {
            return ['action' => 'login', 'origin' => $aid, 'sign' => $sign, 'signIds' => $signIds];
        }
    }

    public function u2fEnroll(Request $request) {
        $this->validate($request, [
            'cid' => 'required|string',
            'u2f' => 'required|string'
        ]);
        $needsEnrollment = Key::where('email', $request->jwt->email)->count() == 0;
        if(!$needsEnrollment) return response(null, 400);
        $ch = Challenge::challengeById($request->get('cid'), $request->jwt->email);
        if($ch == null) return response(null, 404);
        $ch->expires_at = Carbon::now();
        $ch->save();
        $resp = null;
        $er = new RegisterRequest();
        $er->setAppId($this->aid($request));
        $er->setChallenge($this->base64url_encode($ch->challenge));
        $s = $this->server();
        $s->setAppId($this->aid($request));
        $s->setRegisterRequest($er);
        $s->disableCAVerification();
        $res = null;
        try {
            $resp = RegisterResponse::fromJson($request->get('u2f'));
            $res = $s->register($resp);
        } catch(\Exception $e) {
            return response(null, 400);
        }
        $k = Key::create(['id' => Utils::genUUID(), 'email' => $request->jwt->email, 'handle' => base64_encode($res->getKeyHandleBinary()), 'cert' => base64_encode($res->getAttestationCertificateBinary()), 'key' => base64_encode($res->getPublicKey())]);
        Auth::login(User::forEmail($request->jwt->email));
        return response($k, 201);
    }

    public function completeLogin(Request $request) {
        $this->validate($request, [
            'cids' => 'required|array',
            'u2f' => 'required|string'
        ]);
        $s = $this->server();
        $s->setAppId($this->aid($request));
        $s->disableCAVerification();
        $signs = [];
        foreach($request->get('cids') as $c) {
            $f = Challenge::challengeById($c, $request->jwt->email);
            $f->expires_at = Carbon::now();
            $f->save();
            if($f == null) return response(null, 404);
            if($f->handle == null) return response(null, 400);
            $x = new SignRequest();
            $x->setAppId($s->getAppId());
            $x->setChallenge($this->base64url_encode($f->challenge));
            $x->setKeyHandle(base64_decode($f->handle));
            array_push($signs, $x);
        }
        $s->setSignRequests($signs);
        $s->setRegistrations(Utils::getKeysForUser($request->jwt->email));
        try {
            $resp = SignResponse::fromJson($request->get('u2f'));
            $reg = $s->authenticate($resp);
            Auth::login(User::forEmail($request->jwt->email));
            return response(null, 204);
        } catch(\Exception $exception) {
            return response(null, 400);
        }
    }

    public function base64url_encode($data)
    {
        // First of all you should encode $data to Base64 string
        $b64 = base64_encode($data);

        // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
        if ($b64 === false) {
            return false;
        }

        // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
        $url = strtr($b64, '+/', '-_');

        // Remove padding character from the end of line and return the Base64URL result
        return rtrim($url, '=');
    }

    public function base64url_decode($data, $strict = false)
    {
        // Convert Base64URL to Base64 by replacing “-” with “+” and “_” with “/”
        $b64 = strtr($data, '-_', '+/');

        // Decode Base64 string and return the original data
        return base64_decode($b64, $strict);
    }
}

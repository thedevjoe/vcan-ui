<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class UpdateCfCertificates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cf:certupdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates CloudFlare Access Certificates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!env('CF_VERIFY', false)) {
            $this->info("Refusing to update certificates. Verification not enabled.");
            return;
        }
        $c = new Client();
        $resp = $c->get(env('CF_CERT_URI'));
        $keys = json_decode($resp->getBody()->getContents());
        $f = [];
        foreach($keys->public_certs as $k) {
            $f[$k->kid] = $k->cert;
        }
        Storage::disk('local')->put('cf_certs.json', json_encode($f));
        $this->info("Successfully downloaded new certificate set. " . sizeof($f) . ' certificates processed.');
    }
}

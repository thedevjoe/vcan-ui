<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    public $incrementing = false;
    protected $keyType = "string";

    protected $fillable = ['id', 'email', 'challenge', 'handle', 'expires_at'];

    public static function getForEmail($email) {
        $ch = bin2hex(random_bytes(20));
        return Challenge::create(['id' => Utils::genUUID(), 'email' => $email, 'challenge' => $ch, 'expires_at' => Carbon::now()->addMinutes(10)]);
    }

    public static function challengeById($id, $email) {
        $c = Challenge::where('id', $id)->where('email', $email)->where('expires_at', '>', Carbon::now())->first();
        return $c;
    }
}

<?php


namespace App;


use Firehed\U2F\Registration;

class Utils
{
    public static function genUUID() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    public static function getKeysForUser($email) {
        $k = Key::where('email', $email)->get();
        $keys = [];
        foreach($k as $f) {
            $r = new Registration();
            $r->setCounter(0);
            $r->setAttestationCertificate(base64_decode($f->cert));
            $r->setKeyHandle(base64_decode($f->handle));
            $r->setPublicKey(base64_decode($f->key));
            array_push($keys, $r);
        }
        return $keys;
    }
}
